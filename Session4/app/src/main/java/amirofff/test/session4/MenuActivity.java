package amirofff.test.session4;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.widget.Toast;

public class MenuActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_menu);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {

        //add kardan besorate mostaghi dar menu

        menu.add(0 , 100 , 0 , "Contact Us");
        menu.add(0 , 200 , 2 , "Email Us");

        //add kardan az tarighe class

        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.mainmenu, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        //taiine amalkard badaz click
        if (item.getItemId()==100)
            Toast.makeText(this, "Contact Us", Toast.LENGTH_SHORT).show();
        if (item.getItemId()==200)
            Toast.makeText(this, "Email Us", Toast.LENGTH_SHORT).show();
        if (item.getItemId()==R.id.settings)
            Toast.makeText(this, "Settings", Toast.LENGTH_SHORT).show();
        if (item.getItemId()==R.id.signup)
            Toast.makeText(this, "SignUp", Toast.LENGTH_SHORT).show();
        



        return super.onOptionsItemSelected(item);
    }
}
