package amirofff.test.session4.models;

/**
 * Created by macbook on 6/13/2017 AD.
 */

public class ChannelModel {

    String channelName;
    String channelLogoURL;
    int id;

    public ChannelModel(String channelName, String channelLogoURL, int id) {
        this.channelName = channelName;
        this.channelLogoURL = channelLogoURL;
        this.id = id;
    }

    public String getChannelName() {
        return channelName;
    }

    public void setChannelName(String channelName) {
        this.channelName = channelName;
    }

    public String getChannelLogoURL() {
        return channelLogoURL;
    }

    public void setChannelLogoURL(String channelLogoURL) {
        this.channelLogoURL = channelLogoURL;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }
}
