package amirofff.test.session4.models;

import com.orm.SugarRecord;

/**
 * Created by macbook on 6/22/2017 AD.
 */

public class StudentModel extends SugarRecord<StudentModel> {

    String name;
    String family;
    String mobile;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getFamily() {
        return family;
    }

    public void setFamily(String family) {
        this.family = family;
    }

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public StudentModel() {
    }

    public StudentModel(String name, String family, String mobile) {
        this.name = name;
        this.family = family;
        this.mobile = mobile;
    }
}
